/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use serde::{Deserialize, Serialize};

use crate::Annotated;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Expression {
    Hole(Annotated<Hole>),
    Lit(Annotated<Literal>),
    Identifier(Annotated<Identifier>),
    Call {
        func: Box<Annotated<Expression>>,
        args: Vec<Annotated<Expression>>,
    },
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Identifier {
    Name(String),
    Operator(Operator),
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Literal {
    Number(u64),
    Str(String),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Hole {
    pub id: u64,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Operator {
    Add,
}

#[cfg(feature = "helper-functions")]
pub use self::helpers::*;

#[cfg(feature = "helper-functions")]
pub mod helpers {

    use crate::ann_;
    use crate::expression::{Expression, Hole, Identifier, Literal, Operator};
    use std::borrow::Borrow;

    pub fn hole(id: u64) -> Expression {
        Expression::Hole(ann_(Hole { id }))
    }

    pub fn lit(l: Literal) -> Expression {
        Expression::Lit(ann_(l))
    }

    pub fn name(n: impl Into<String>) -> Identifier {
        Identifier::Name(n.into())
    }

    pub fn op(operator: Operator) -> Identifier {
        Identifier::Operator(operator)
    }

    pub fn ident(n: impl Into<String>) -> Expression {
        Expression::Identifier(ann_(name(n)))
    }

    pub fn call<Args>(f: Expression, arguments: Args) -> Expression
    where
        Args: IntoIterator,
        Args::Item: Borrow<Expression>,
    {
        Expression::Call {
            func: Box::new(ann_(f)),
            args: arguments
                .into_iter()
                .map(|x| ann_(x.borrow().clone()))
                .collect(),
        }
    }

}
