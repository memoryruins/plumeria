/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use serde::{Deserialize, Serialize};

use crate::expression::{Expression, Identifier};
use crate::Annotated;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Function {
    pub identifier: Identifier,

    pub parameters: Vec<Annotated<Parameter>>,

    pub body: Annotated<Expression>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Parameter {
    pub name: Identifier,
    pub type_: Annotated<Type>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Type {
    pub name: Identifier,
}
