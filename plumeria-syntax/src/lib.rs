/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use serde::{Deserialize, Serialize};

pub mod expression;
pub mod item;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Annotation(pub String);

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Annotated<T> {
    #[serde(skip_serializing_if = "Option::is_none", default)]
    pub annotation: Option<Annotation>,
    #[serde(flatten)]
    pub item: T,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Module {
    pub functions: Vec<Annotated<item::Function>>,
}

#[cfg(feature = "helper-functions")]
pub use self::helpers::*;

#[cfg(feature = "helper-functions")]
pub mod helpers {

    use crate::{Annotated, Annotation};

    pub fn ann_<T>(item: T) -> Annotated<T> {
        Annotated {
            annotation: None,
            item,
        }
    }

    pub fn ann<A, T>(annotation: A, item: T) -> Annotated<T>
    where
        A: Into<String>,
    {
        Annotated {
            annotation: Some(Annotation(annotation.into())),
            item,
        }
    }

}
