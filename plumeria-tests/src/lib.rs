/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#[cfg(test)]
mod tests {
    use plumeria_syntax::expression::*;

    #[test]
    fn test_lit() {
        // make this actually do something lol
        let expr = lit(Literal::Number(42));

        dbg!(expr);
    }

    #[test]
    fn test_ser_de() {
        // could probably use some quickcheck over here

        let expr = call(
            ident("f"),
            &[lit(Literal::Number(4)), lit(Literal::Number(5)), hole(42)],
        );

        let text = serde_json::to_string_pretty(&expr).unwrap();

        eprintln!("{}", text);

        let de_expr: Expression = serde_json::from_str(&text).unwrap();

        assert_eq!(expr, de_expr);
    }
}
